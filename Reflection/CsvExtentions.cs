﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Reflection
{
	public static class CsvExtentions
	{
		public static string ToCsv(this object obj)
		{
			return string.Join(
				Environment.NewLine,
				obj
					.GetType()
					.GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
					.Select(field => $"{field.Name};{field.GetValue(obj)}"));
		}

		public static T FromCsv<T>(this string str) where T : new()
		{
			T result = new T();
			var type = result.GetType();
			var fields = str
				.Split(Environment.NewLine)
				.Select(f => f.Split(';'))
				.ToDictionary(x => x[0], x => x[1]);

			foreach (var key in fields.Keys)
			{
				var propertyInfo = type.GetField(key, BindingFlags.NonPublic | BindingFlags.Instance);
				propertyInfo.SetValue(result, int.Parse(fields[key]));
			}

			return result;
		}

	}
}
