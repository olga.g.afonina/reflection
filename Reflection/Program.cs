﻿using Newtonsoft.Json;
using System;

namespace Reflection
{
	class F
	{
		int i1, i2, i3, i4, i5;
		public F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
	}

	class Program
	{
		static void Main(string[] args)
		{
			var f = new F().Get();
			var serialization = f.ToCsv();

			var t1 = DateTime.UtcNow;
			for (int i = 0; i < 10_000; i++)
			{
				f.ToCsv();
			}
			var t2 = DateTime.UtcNow;
			Console.WriteLine(t2 - t1);

			t1 = DateTime.UtcNow;
			for (int i = 0; i < 10_000; i++)
			{
				serialization.FromCsv<F>();
			}
			t2 = DateTime.UtcNow;
			Console.WriteLine(t2 - t1);

			// Newtonsoft JSON сериализация
			t1 = DateTime.UtcNow;
			for (int i = 0; i < 10_000; i++)
			{
				JsonConvert.SerializeObject(f);
			}
			t2 = DateTime.UtcNow;
			Console.WriteLine(t2 - t1);

			var output = JsonConvert.SerializeObject(f);
			// Newtonsoft JSON десериализация
			t1 = DateTime.UtcNow;
			for (int i = 0; i < 10_000; i++)
			{
				JsonConvert.DeserializeObject<F>(output);
			}
			t2 = DateTime.UtcNow;
			Console.WriteLine(t2 - t1);
			Console.ReadLine();
		}
	}
}